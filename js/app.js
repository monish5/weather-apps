(function($, document, window) {

    $(document).ready(function() {

        // Cloning main navigation for mobile menu
        $(".mobile-navigation").append($(".main-navigation .menu").clone());

        // Mobile menu toggle 
        $(".menu-toggle").click(function() {
            $(".mobile-navigation").slideToggle();
        });
    });

    $(window).load(function() {

    });

})(jQuery, document, window);

// if(navigator.geolocation){
//     navigator.geolocation.getCurrentPosition(function(pos){
//         console.log(pos);
//     });
// }

const dayName=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

const monthName=['Jan','Feb','Mar','Apr','May','June','July','Aug','Sept','Oct','Nov','Dec'];

const btnFind=document.getElementById('btnFind');
const cityElement= document.getElementById('city');

const dayElements=document.getElementsByClassName('day');
const tempElements=document.getElementsByClassName('temp');

const icons=document.getElementsByClassName('weather-icon');

const defaultCity='Mumbai';
let cityName;

btnFind.addEventListener('click',function(evt){
    evt.preventDefault();
    getCityName();
    getAndDisplayWeatherDetails(cityName);
});

const getCityName=()=>{
    cityName=cityElement.value.length===0 ? defaultCity:cityElement.value ;
    console.log(cityName);
}
function getAndDisplayWeatherDetails(cityName){
    getForecast(cityName)
    .then(data=>{
        console.log("inn");
        processReceivedData(data)}
        )
    .catch(err=>alert("Something Went Wrong : "+err));
}

function processReceivedData(data){
    console.log("Mooo");
    data=data.list;
    document.getElementById("location").innerHTML=cityName;
    const imagePath="images/icons";
    for(let i=0,j=0;i<data.length;i+=8,j++){
        console.log(j+" "+i);
        tempElements[j].innerHTML=Math.round(data[i].main.temp)+"<sup>o</sup>C";
        const dateObj=new Date(data[i].dt_txt);
        const day=dayName[dateObj.getDay()];
        const month=monthName[dateObj.getMonth()];
        dayElements[j].innerHTML=day;
        console.log("datess");
        if(j==0){
            document.getElementById("date").innerHTML=dateObj.getDate()+" "+month;
            
            document.getElementById("humidity").innerHTML=Math.round(data[i].main.humidity)+" %";

            document.getElementById("wind-speed").innerHTML=Math.round(data[i].wind.speed*3.6)+" km/h";

            document.getElementById("wind-degree").innerHTML=Math.round(data[i].wind.deg)+"<sup>o</sup>";
        }
        icons[j].src=`${imagePath}/${data[i].weather[0].icon}.svg`;
        
    }
}

