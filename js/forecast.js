//6e8abe4a012f87cdc8c0a09680b8ca8f
const API_KEY='6e8abe4a012f87cdc8c0a09680b8ca8f';
const BASE_URL='http://api.openweathermap.org/data/2.5/forecast';
const units={
    farhen:'imperial',
    celsius:'metric',
    default:'standard'
};

const getForecast=async(cityName)=>{
    console.log("hii");
    const URL=BASE_URL+`?q=${cityName}&appid=${API_KEY}&units=${units.celsius}`;
    const res=await fetch(URL);
    if(res.ok){
        console.log("getff");
        const data=await res.json();
        return data;
    }
    throw new Error(res.status);
}

getForecast('Mumbai').then(data=>{
    console.log(data);
})
.catch(err=>console.warn(err));
